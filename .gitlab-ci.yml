stages:
  - remote_setup
  - test
  - code_analysis
  - deploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PRE_COMMIT_HOME: "${CI_PROJECT_DIR}/.cache/pre-commit"

cache:
  paths:
    - .cache/pip
    - ${PRE_COMMIT_HOME}
    - venv/


# https://gitlab.com/gitlab-org/gitlab/-/issues/33694#note_335120563
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always


setup:
  stage: remote_setup
  script:
    - virtualenv venv -p ~/Python-3.7.9/python
    - source venv/bin/activate
    - python -V
    - python -m pip install --upgrade pip
    - python -m pip install --upgrade setuptools
    - python -m pip install --upgrade twine
    - python -m pip install -r requirements-dev.txt


unittests:
  stage: test
  before_script:
    - source venv/bin/activate
    - python -V
  script:
    - coverage run -m pytest datafold/
    - coverage html -d ./coverage/
    - coverage report
  allow_failure: false
  artifacts:
    paths:
      - ./coverage/
    expire_in: 1 week
    when: on_success


functional_tests:
  before_script:
    - source venv/bin/activate
    - python -V
  script:
    - export PYTHONPATH=`pwd`
    - echo $PYTHONPATH
    - python -m pytest tutorials/
  allow_failure: false


code_checks:
  stage: code_analysis
  before_script:
    - source venv/bin/activate
    - python -V
  script:
    - pre-commit run --all
  allow_failure: true


docu_check:
  stage: code_analysis
  before_script:
    - source venv/bin/activate
    - python -V
  script:
    - cd doc/ && make html
  except:
    - master
  allow_failure: false


pages:
  stage: deploy
  before_script:
    - source venv/bin/activate
    - python -V
  script:
    - export DATAFOLD_NBSPHINX_EXECUTE=always
    - sphinx-apidoc -f -o ./doc/source/_apidoc/ ./datafold/
    - sphinx-build -b html ./doc/source/ ./public/
  artifacts:
    paths:
    - public
  rules:
    # from: https://forum.gitlab.com/t/how-to-setup-manual-job-from-feature-branch-that-runs-automatically-on-master/38892
    # always update pages if on master
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    # manual option if push to branches other than master
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: manual

# Requires to set the variables TWINE_USERNAME and TWINE_PASSWORD in gitlab CI/CD
pypi_upload:
  stage: deploy
  before_script:
    - source venv/bin/activate
  script:
    - python setup.py sdist bdist_wheel
    - twine check dist/*
    - twine upload --verbose dist/*
  only:
    - master
  when: manual
