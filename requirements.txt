findiff==0.8.9
matplotlib>=3.2.0,<3.4.0
numexpr>=2.7.1,<3.0.0
numpy>=1.18.0,<1.21.0
pandas>=1.2.0,<1.3.0
scikit-learn>=0.24.1,<0.25.0
scipy>=1.4.0,<1.7
